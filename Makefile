
shell: client.c server.c
	gcc client.c -std=c99 -lm -o client
	gcc server.c -std=c99 -lm -o server
clean:
	rm -f *.o client.c~ server.c~ Makefile~ client server ReadMe.txt~
