#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void sieve(short primes[], int max) {

  //Store and print current prime
  int myPrime = primes[0];
  //printf("Current Prime is %d \n", myPrime);

  //Go through all of the multiples                   
  for(int mult = 2; (mult*myPrime) <= max; mult++){
    //Mark the multiples as not prime                       
    primes[mult*myPrime] = 1;
  }
  
  //print the loop
  //for(int i =0; i<max+1; i++) {
  //  printf("%d", primes[i]);
  // }
  
  //Move one index                                       
  myPrime++;

  //Move to next prime                   
  while(primes[myPrime] == 1) {
    myPrime++;
  }
  
  //Update the next prime to start on
  primes[0] = myPrime;

}

void printAllPrimes(short primes[], int max) {

  //print all of the primes                                                       
  for(int i = 0; i < max; i++) {
    if(primes[i] == 0){
      printf("%d,", i);
    }
  }

}

void printPrimes(short primes[], int max) {
  int numPrimes=0;
  //find the number of primes                                                       
  for(int k=0; k<max+1; k++){
    if(primes[k] == 0) {
      numPrimes++;
    }
  }
  //check to see if primes are less than 8                                          
  if(numPrimes < 8) {
    printAllPrimes(primes, max);
  } else{
    int count = 0;
    //grab the first 2 primes                                                         
    for(int i=1; count<2; i++){

      if(primes[i] == 0) {
	printf("%d,", i);
        count++;
      }
    }
    printf("...");
    count = 0;
    short tArray[2];
    //Grab the last 2 primes                                                          
    for(int j=max; count<2; j--) {

      if(primes[j] == 0 && count == 0){
	tArray[0] = j;
	count++;
      }else if(primes[j] == 0 && count == 1){
	tArray[1] = j;
	count++;
      }
    }
    printf("%d,", tArray[1]);
    printf("%d\n", tArray[0]);
  }
}



int main() {
  
  //create the server socket
  int server_socket;
  server_socket = socket(AF_INET, SOCK_STREAM, 0);

  
  //define the server address
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(9555);
  server_address.sin_addr.s_addr = INADDR_ANY;
  
  // bind the socket to our specified IP and Port
  bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));
  
  listen(server_socket, 5);

  int client_socket;
  client_socket = accept(server_socket, NULL, NULL);
  if(client_socket < 0){
    perror("ERROR on accept");
  }
  
  //define the maximum
  int max;
  
  if(recv(client_socket, &max, sizeof(max), 0) < 0){
    perror("Error sending max to socket");
  }

  //Declare the array for the sieve
  short primes[max+1];
  memset(primes, 0, (max+1) * sizeof(short));
    
  //go through all the numbers until you reach the end maxSqrt
  while(primes[0] < sqrt((double)max)) {
    //accept the primes from the client 
    recv(client_socket, &primes, sizeof(primes), 0);
    printf("Recd: ");
    printPrimes(primes,max);

    //run the sieve
    sieve(primes,max);
    
    printf("Sent:");
    printPrimes(primes,max);
    //send the array back to the client
    if((primes[0] != 1) && (primes[0] != 0)) {
      send(client_socket, &primes, sizeof(primes), 0);
    }
    printf("To: Client \n");
  }


  // send the message
  //send(client_socket, server_message, sizeof(server_message), 0);

  // close the socket
  close(server_socket);
  
  return 0;
} 
