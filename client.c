#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <math.h>

void sieve(short primes[], int max) {

  //Store and print current prime
  int myPrime = primes[0];
  //printf("Current Prime is %d \n", myPrime);


  //Go through all of the multiples
  for(int mult = 2; (mult*myPrime) <= max; mult++){
    // Mark the multiples as not prime
    primes[mult*myPrime] = 1;
  }

  /*
  //Print out the loop
  for(int i=0; i<max+1; i++) {
     printf("%d", primes[i]);
  }
  */
 
  //Move to next index
  myPrime++;
  
  //Move to next prime
  while(primes[myPrime] == 1) {
    myPrime++;
  }

  primes[0] = myPrime;
  
}

void printAllPrimes(short primes[], int max) {
  
    //print all of the primes
  for(int i = 0; i < max; i++) {
    if(primes[i] == 0){
      printf("%d, ", i);
    }
  }

  printf("\n");
}

void printPrimes(short primes[], int max) {
  int numPrimes=0;
  //find the number of primes
  for(int k=0; k<max+1; k++){
    if(primes[k] == 0) {
      numPrimes++;
    }
  }
  //check to see if primes are less than 8
  if(numPrimes < 8) {
    printAllPrimes(primes, max);
  } else{
  int count = 0;
  //grab the first 2 primes
  for(int i=1; count<2; i++){
    
    if(primes[i] == 0) {
      printf("%d,", i);
	count++;
    }
  }
  printf("...");
  count = 0;
  short tArray[2];
  //Grab the last 2 primes
  for(int j=max; count<2; j--) {
    
    if(primes[j] == 0 && count == 0){
      tArray[0] = j;
      count++; 
    }else if(primes[j] == 0 && count == 1){
      tArray[1] = j;
      count++;
    }
  }
  printf("%d,", tArray[1]);
  printf("%d\n", tArray[0]);
  }
}


int main(int argc, char *argv[]) {
  //create the socket
  int network_socket;
  network_socket = socket(AF_INET,SOCK_STREAM, 0);
  
  //specify an adress for the socket
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(9555);
  server_address.sin_addr.s_addr = inet_addr(argv[1]);

  int connection_status = connect(network_socket, (struct sockaddr *) &server_address, sizeof(server_address));
  // check for error with the connection
  if(connection_status == -1) {
    printf("There was an error making a connection to the remote socket\n");
  }

  // take in the max
  int max = atoi(argv[2]);

  if(send(network_socket, &max, sizeof(max),0) < 0){
    perror("Error sending to the socket");
  }
  

  short primes[max+1];
  memset(primes, 0, (max+1) * sizeof(short));

  primes[0] = 2;
  primes[1] = 1;  
  
  while(primes[0] < sqrt((double)max)) {
    //run the sieve
    sieve(primes,max);
    
    //Print primes and send to server
    printf("Sent: ");
    printPrimes(primes, max);
    printf("To: %s\n", argv[1]);
    if((primes[0] != 1) && (primes[0] != 0)) {
      send(network_socket, &primes, sizeof(primes),0);
    }
    
    //recieve primes back from server
    recv(network_socket, &primes, sizeof(primes),0);
    printf("Recvd:");
    printPrimes(primes, max);
    
  }
  printf("Full List of Primes:\n");
  printAllPrimes(primes, max);
    
  // Close the socket
  close(network_socket);
  return 0;
}
