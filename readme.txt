To compile the program:

run "make"

This will compile the binary and create "client" and "server"

To Clean:

Simply run "make clean"

To Run:
You must be logged into two different thing machines (ex thing1 and thing3). On the thing you plan to use for the server, run "ifconfig" and make note of the ip address listed under the first instance of "inet". Then run "./server" to start the server. 
On the next thing machine, you will run "./client" with 2 arguements, the first being the IPAddress of the thing the server is running on and the second being the size of the sieve you want to run. Ex. "./client 10.35.195.46 1000"

     

